---
layout: home
title: home
nav_order: 1
mathjax: true
---

<div style="display: flex; justify-content: space-between;">
    <div style="flex: 1; padding-right: 10px;">
        <img src="{{ site.url }}{{ site.baseurl }}/fet.png" alt="head"/>
    </div>
    <div style="flex: 1; padding-left: 10px;">
        Hello there, my name is Alan! I'm a rising 2nd year Master's student in the Center for Bits and Atoms (CBA) in the MAS dept at MIT, and I'm exploring discrete electronics assembly.
    </div>
</div>

