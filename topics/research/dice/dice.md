---
layout: default
title: dice
mathjax: true
parent: research
has_children: true
---

# dice
{: .no_toc }

we're trying to build a discretely assembled computer out of electronic lego bricks.
discrete electronics project, aiming to assemble a 10,000-part computer, discretely and automatically.
 
- a new way to breadboard
- COTS components are approaching early lithography feature sizes
- alternative to recycling e-waste

# trajectory
- depth axis: how does this electronics geometry scale to represent complex circuitry?
- breadth axis: how general purpose is this electronics geometry?

# mm -> um -> nm

this computer will be built using mm-scale parts, but the roadmap eventually leads us to nm parts.
