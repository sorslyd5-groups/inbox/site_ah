---
layout: post
title: tiles
parent: dice
grand_parent: research
---

# o_power batch 1

- sent out 7 boards for fab, 6 tiles and 1 macro-tile
- rotational restrictions due to lines of symmetry, but interesting workaround by flipping

| VCC                                                                 | GND                                                                 | I/O                                                                 |
| ------------------------------------------------------------------- | ------------------------------------------------------------------- | ------------------------------------------------------------------- |
| ![]({{ site.url }}{{ site.baseurl }}/topics/research/media/remote/2024.03.27/tiles/Screenshot_2024-03-27_132119.png) | ![]({{ site.url }}{{ site.baseurl }}/topics/research/media/remote/2024.03.27/tiles/Screenshot_2024-03-27_132243.png) | ![]({{ site.url }}{{ site.baseurl }}/topics/research/media/remote/2024.03.27/tiles/Screenshot_2024-03-27_132411.png) |
| 1206 (2-pin)                                                        | SOT-23 (3-pin)                                                      | cross (4-way)                                                       |
| ![]({{ site.url }}{{ site.baseurl }}/topics/research/media/remote/2024.03.27/tiles/Screenshot_2024-03-27_132352.png) | ![]({{ site.url }}{{ site.baseurl }}/topics/research/media/remote/2024.03.27/tiles/Screenshot_2024-03-27_132229.png) | ![]({{ site.url }}{{ site.baseurl }}/topics/research/media/remote/2024.03.27/tiles/Screenshot_2024-03-27_132309.png) |

![]({{ site.url }}{{ site.baseurl }}/topics/research/media/remote/2024.03.27/nets.png)

| 8x8 macro-tile                                                      |                                                                      |
| ------------------------------------------------------------------- | -------------------------------------------------------------------- |
| ![]({{ site.url }}{{ site.baseurl }}/topics/research/media/remote/2024.03.27/tiles/Screenshot_2024-03-27_132333.png) | ![]({{ site.url }}{{ site.baseurl }}/topics/research/media/remote/2024.03.27/circuits/PXL_20240327_164900041.cmp.jpg) |


