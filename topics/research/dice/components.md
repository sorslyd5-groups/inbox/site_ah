---
layout: post
title: components
parent: dice
grand_parent: research
---

# h_eon cq pipeline

h-connector, eye-of-the-needle geometry, cadquery pipeline towards building CI/CD CAD geometry

| 8-up carrier for h_eon geometry                                              |
| ---- |
| ![h_eon]({{ site.url }}{{ site.baseurl }}/topics/research/media/remote/2024.04.02/Screenshot_2024-04-02_112728.cmp.png) |
 
- spent time refactoring h_eon cadquery code for better separation of concerns
- parametric sweeping and cloning
- tuned process to find good balance for reliable fablight fabrication

# claw gripper
 
# feeder

# form tool
