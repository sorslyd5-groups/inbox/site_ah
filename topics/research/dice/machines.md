---
layout: post
title: machines
parent: dice
grand_parent: research
---

[#](#) background

- we're trying to build a discretely assembled computer out of electronic lego bricks
- a new way to breadboard
- COTS components are approaching early lithography feature sizes
- alternative to recycling e-waste

| discrete risc-v computer                                              |
| ---- |
| ![risc]({{ site.url }}{{ site.baseurl }}/topics/research/media/remote/2024.04.02/Screenshot_2024-04-02_122637.cmp.png) |

## machines

- demonstrated DICE pickup using lumenpnp
- began assembly of danke machine for DICE pnp


| lumenpnp (danke v1)                                                | clank low-slung (danke v2)                                                |
| ------------------------------------------------------------------ | ------------------------------------------------------------------------- |
| ![]({{ site.url }}{{ site.baseurl }}/topics/research/media/remote/2024.03.27/machine/photo_2024-03-27_13-09-08.jpg) | ![]({{ site.url }}{{ site.baseurl }}/topics/research/media/remote/2024.03.27/machine/Screenshot_2024-03-07_122059.png)<br> |
| ![]({{ site.url }}{{ site.baseurl }}/topics/research/media/remote/2024.03.27/machine/photo_2024-03-27_13-07-42.jpg) | ![]({{ site.url }}{{ site.baseurl }}/topics/research/media/remote/2024.03.27/machine/photo_2024-03-27_13-07-40.jpg)        |
|                                                                    |                                                                           |
