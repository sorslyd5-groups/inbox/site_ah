# Power electronics

live site for working on page:
https://power-electronics-sorslyd5-groups-inbox-517c2945352f152cc7e6318.gitlab.io/

documentation for the template:
https://just-the-docs.github.io/just-the-docs/

# why
 
since the cba gitlab instance can't build sites using static site generators because no backend execution (and won't for the time being), I've made this repo using the same template Jake used for his mechanical lecture and set it up such that changes can be deployed to the above live site link.

# how to dev locally

copied verbatim from Jake

to develop locally, pull the repo, install ruby, and install bundle using
`bundle install`

then run
`bundle exec jekyll serve`

# plan

I'm thinking we can pick some topics to take a first stab at them, then we can check each others pages and make additional edits in a second pass.
I've put my name (Alan) down next to transistors and thermals.

when we're satisfied, we can build locally and c/p the \_site directory contents to the power_electronics topic in the machine class site repo.

## Content

nice simulations (can be inserted as an iframe):
https://www.falstad.com/circuit/e-nmosfet.html


- passives
    - resistors
    - capacitors
    - inductors
    - diodes
- Transistors - `Alan`
    - comparison
    - BJT
    - FET
- AC vs DC
    - comparison
    - AC to DC
    - DC to AC
- regulators
    - voltage divider
    - LDO
    - Zener reference
    - switching regulators
- drivers
    - DC motor: H-bridge
    - BLDC: tri-phase
    - stepper: current sensing
    - current sensing
- thermals - `Alan`
    - passive vs active
        - convection
    - packaging decisions (pcb weight, wire gauge, connectors)
    - simulations
        - two-resistor model

external links to pages and papers

## notes from class

- power electronics lecture keypoints that need to be (could be) covered
    - understand how to read datsheet for a transistor
    - not all of the specs are always there
    - when to go beyond mosfets
        - igbt, back emf sensing
        - back emf sensing ; how to drive motors by sensing
        - sinsuoidal 
        - solidstate transformers applicable for the grid
            - turn everything in dc ac then dc (dc to dc), 98% efficient
    - drivers
        - chopoper drivers
    - types of power supplies
    - charge pumps
    - types of amplifiers
    - cooling technques
